# Homework

## File Upload:

| Test step                                  | Result                                  |
|--------------------------------------------|-----------------------------------------|
| Open the website https://www.flickr.com    | Ok                                      |
| Log in for an account                      | Ok, log in                              |
| Click on the "Upload" button               | Ok, new window is opened                |
| Upload an invalid file type (such as .pdf) | "This doesn't look like a valid photo." |

### Results:

![img_1.png](img_1.png)

The site uploads invalid photo. However, it shows the attention message. Therefore, some dangerous file can be uploaded.

## Forget password:

| Test step                               | Result                         |
|-----------------------------------------|--------------------------------|
| Open the website https://www.flickr.com | Ok                             |
| Click on "Log in" button                | Ok, new window is opened       |
| Write email                             | Ok, new window is opened       |
| Click on "Forget password" button       | Ok, new window is opened       |
| Do check for captcha                    | Ok                             |
| Invalid email                           | "Invalid email or password."   |
| Valid email                             | "Check your inbox"             |

### Results:
Invalid: \
![img_2.png](img_2.png)

Valid: \
![img_3.png](img_3.png)

It is possible to get list of users emails.

## Insecure Direct Object Reference Prevention:

| Test step                               | Result                   |
|-----------------------------------------|--------------------------|
| Open the website https://www.flickr.com | Ok                       |
| Click on "You" button                   | Ok, new window is opened |
| Click on "About" button                 | Ok, new window is opened |
| Change the identifier of page           | Not Found                |
| Change the identifier of page           | Found user               |

### Results:
![img_4.png](img_4.png)
![img_5.png](img_5.png)

It is possible to find all possible users due to absence of hash of identifier.

## XSS Filter Evasion:

| Test step                                                   | Result                                                                                                       |
|-------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------|
| Open the website https://www.flickr.com                     | Ok                                                                                                           |
| Click on "Search" bar                                       | Ok, new window is opened                                                                                     |
| Write xss injection "<IMG SRC= onmouseover="alert('xxs')">" | "Oops! There are no matches for “<IMG SRC= onmouseover="alert('xxs')">”. Please try broadening your search." |

### Results:
![img_7.png](img_7.png)

Site has defense against some XSS JavaScript injection.